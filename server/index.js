const express = require('express');
const app = express();
const path = require('path');

const services = require('./services');
const port = 8087;

const publicWeb = path.join(process.cwd(), 'dist');

app.get('/ping', services.visitors.startCount);
app.use(express.static(path.join(publicWeb)));

app.get('', async (req, res) => {
  console.log('publicweb', publicWeb);
  res.sendFile('index.html', { root: publicWeb });
});

app.listen(port, () => {
  console.log(`site running on http://localhost:${port}`);
});
