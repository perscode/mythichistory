const fs = require('fs');
const path = require('path');
const filePath = path.join(process.cwd(), 'server', 'visitor-count.json');
const file = JSON.parse(fs.readFileSync(filePath).toString());

function startCount(req, res, next) {
  const ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
  if (!file[ip]) {
    file[ip] = 0;
  }
  file[ip]++;
  const keys = Object.keys(file).length + 1;
  const visitors = [];
  for (let key of Object.keys(file)) {
    visitors.push(file[key]);
  }

  fs.writeFileSync(filePath, JSON.stringify(file));
  res.json(visitors);
}

module.exports = {
  startCount,
};
