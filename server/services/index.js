const raiderIO = require('./raider-io.service');
const visitors = require('./visitors.service');

module.exports = {
  raiderIO: raiderIO,
  visitors: visitors,
};
