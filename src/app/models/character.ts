import { VirtualTimeScheduler } from "rxjs";

export class Character {
    name: string;
    realm: string;
    region: string

    constructor(name?: string, realm?: string, region?: string) {
        this.name = name? name : '';
        this.realm = realm? realm : '';
        this.region = region? region : '';
    }
};
