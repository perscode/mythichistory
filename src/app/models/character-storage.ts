import { Character } from './character';

export class CharacterStorage {
  constructor() {}

  addCharacter(char: Character) {
    const characters = this.getCharacters();
    const exist = characters.some((savedChar: Character) => savedChar.name === char.name && savedChar.realm === char.realm);
    if (!exist) {
      characters.push(char);
      localStorage.setItem('characters', JSON.stringify(characters));
      return true;
    }
    return false;
  }

  deleteCharacter(char: Character) {
    const characters = this.getCharacters();
    const index = characters.findIndex(
      (charData: any) => charData.name === char.name && charData.realm === char.realm && charData.region === char.region
    );
    characters.splice(index, 1);
    this.saveCharacters(characters);
  }

  getCharacters() {
    const storage = localStorage.getItem('characters');
    return storage ? JSON.parse(storage) : [];
  }

  saveCharacters(characters: Character[]) {
    localStorage.setItem('characters', JSON.stringify(characters));
  }
}
