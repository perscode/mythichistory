import { Character } from './character';
import { MythicPlusRun, RaiderIOApiData } from './raiderIO-api-data';
import { REWARDS } from '../models/rewards';

export class MythicHistory {
  name: string;
  realm: string;
  region: string;
  ilvlSlots: number[];
  highestRun: number;
  runs: Array<MythicPlusRun>;

  constructor(raiderIOData: RaiderIOApiData) {
    this.runs = raiderIOData.mythic_plus_weekly_highest_level_runs;
    this.name = raiderIOData.name;
    this.realm = raiderIOData.realm;
    this.region = raiderIOData.region;
    this.ilvlSlots = [];
    this.highestRun = this.getHighestRun(this.runs);
  }

  getTop10(char: RaiderIOApiData): Array<number> {
    return [0];
  }

  getHighestRun(runs: MythicPlusRun[]) {
    const sortedRuns = runs.sort((a, b) => b.mythic_level - a.mythic_level);
    return sortedRuns[0]?.mythic_level;
  }
}

class MythicRun {}
