export interface MythicRunReward {
    level: number;
    item: {EndOfRun: number, WeeklyReward: number};
}

export const REWARDS: Array<MythicRunReward> = [
    {
        "level": 1,
        "item": {
            "EndOfRun": 183,
            "WeeklyReward": 0
        }
    },
    {
        "level": 2,
        "item": {
            "EndOfRun": 187,
            "WeeklyReward": 200
        }
    },
    {
        "level": 3,
        "item": {
            "EndOfRun": 190,
            "WeeklyReward": 203
        }
    },
    {
        "level": 4,
        "item": {
            "EndOfRun": 194,
            "WeeklyReward": 207
        }
    },
    {
        "level": 5,
        "item": {
            "EndOfRun": 194,
            "WeeklyReward": 210
        }
    },
    {
        "level": 6,
        "item": {
            "EndOfRun": 197,
            "WeeklyReward": 210
        }
    },
    {
        "level": 7,
        "item": {
            "EndOfRun": 200,
            "WeeklyReward": 213
        }
    },
    {
        "level": 8,
        "item": {
            "EndOfRun": 200,
            "WeeklyReward": 216
        }
    },
    {
        "level": 9,
        "item": {
            "EndOfRun": 200,
            "WeeklyReward": 216
        }
    },
    {
        "level": 10,
        "item": {
            "EndOfRun": 204,
            "WeeklyReward": 220
        }
    },
    {
        "level": 11,
        "item": {
            "EndOfRun": 204,
            "WeeklyReward": 220
        }
    },
    {
        "level": 12,
        "item": {
            "EndOfRun": 207,
            "WeeklyReward": 223
        }
    },
    {
        "level": 13,
        "item": {
            "EndOfRun": 207,
            "WeeklyReward": 223
        }
    },
    {
        "level": 14,
        "item": {
            "EndOfRun": 207,
            "WeeklyReward": 226
        }
    },
    {
        "level": 15,
        "item": {
            "EndOfRun": 210,
            "WeeklyReward": 226
        }
    }
]