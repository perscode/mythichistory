
export interface Affix {
    description: string;
    id: number;
    name: string;
    wowhead_url: string;
}

export interface MythicPlusRun {
    dungeon: string;
    short_name: string;
    mythic_level: number;
    completed_at: string;
    clear_time_ms: number;
    num_keystone_upgrades: number;
    map_challenge_mode_id: number;
    score: number;
    affixes: Array<Affix>;
    url: string;
}

export interface RaiderIOApiData {
    achievement_points: number;
    active_spec_name: string;
    active_spec_role: string;
    class: string;
    faction: string;
    gender: string;
    honorable_kills: number;
    last_crawled_at: string;
    //mythic_plus_recent_runs: Array<MythicPlusRun>;
    mythic_plus_weekly_highest_level_runs: Array<MythicPlusRun>;
    name: string;
    profile_banner: string;
    profile_url: string;
    race: string;
    realm: string;
    region: string;
    thumbnail_url: string;
}