import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';

import { RaiderIOApiData } from '../models';
import { Character } from '../models/character';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {
  constructor(private http: HttpClient) { }

  getMythicHistory(char: Character) {
    const url: string = 'https://raider.io/api/v1/characters/profile?' +
                         `region=${char.region}&` +
                         `realm=${char.realm}&` +
                         `name=${char.name}&` +
                         'fields=mythic_plus_weekly_highest_level_runs';
    return this.http.get<RaiderIOApiData>(url);
  }
}
