import { Component, Input, OnInit, Output } from '@angular/core';
import { inject } from '@angular/core/testing';
import { forkJoin } from 'rxjs';
import { map } from 'rxjs/operators';
import { MythicHistory, RaiderIOApiData } from '../models';
import { Character } from '../models/character';
import { CharacterStorage } from '../models/character-storage';

import { ConfigService } from './config.service';

@Component({
  selector: 'app-config',
  templateUrl: './config.component.html',
  styleUrls: ['./config.component.scss'],
})
export class ConfigComponent implements OnInit {
  displayedColumns: string[] = ['dungeon', 'clear_time_ms', 'completed_at', 'mythic_level'];
  test_Config: Array<Character>;
  mythicHistory: MythicHistory[];
  charName = '';
  realm = 'Draenor';
  region = 'eu';
  charStorage: CharacterStorage;
  error: string = '';

  constructor(private configService: ConfigService) {
    this.mythicHistory = [];
    this.charStorage = new CharacterStorage();
    this.test_Config = this.charStorage.getCharacters();
  }

  ngOnInit(): void {
    this.getMythicHistory(this.test_Config);
  }

  getMythicHistory(chars: Array<Character>) {
    const promises: any = [];
    chars.forEach((char: Character) => {
      promises.push(this.configService.getMythicHistory(char));
    });
    forkJoin(promises).subscribe((response: any) => {
      response.forEach((o: RaiderIOApiData) => {
        this.mythicHistory.push(new MythicHistory(o));
      });
    });
  }

  deleteCharacter(name: string, realm: string, region: string) {
    const char = new Character(name, realm, region);
    const index = this.mythicHistory.findIndex(
      (charData: any) => charData.name === name && charData.realm === realm && charData.region === region
    );
    this.mythicHistory.splice(index, 1);
    this.charStorage.deleteCharacter(char);
  }

  requestUpdate() {
    const newChar = new Character(this.charName, this.realm || 'draenor', this.region || 'eu');
    const added = this.charStorage.addCharacter(newChar);
    if (added) {
      this.configService.getMythicHistory(newChar).subscribe(
        (res) => {
          this.mythicHistory.push(new MythicHistory(res));
          this.error = '';
        },
        (err) => {
          this.error = err?.error?.message || 'an error occurred';
          this.charStorage.deleteCharacter(newChar);
        }
      );
    } else {
      this.error = 'Character already exists';
    }
  }
}
